//
//  PhotoData.h
//  ChallengeFixeAds
//
//  Created by Carlos Manuel Garcia Neto on 23/12/15.
//  Copyright © 2015 Carlos Manuel Garcia Neto. All rights reserved.
//

#import <Foundation/Foundation.h>

@interface PhotoData : NSObject

@property (nonatomic, strong) NSNumber* slot_id;
@property (nonatomic, strong) NSNumber* slot_w;
@property (nonatomic, strong) NSNumber* slot_h;

- (instancetype)init:(NSDictionary*)jsonDict;

@end

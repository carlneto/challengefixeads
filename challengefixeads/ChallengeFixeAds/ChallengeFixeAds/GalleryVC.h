//
//  GalleryVC.h
//  ChallengeFixeAds
//
//  Created by Carlos Manuel Garcia Neto on 26/12/15.
//  Copyright © 2015 Carlos Manuel Garcia Neto. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "Ad.h"

@interface GalleryVC : UIPageViewController<UIPageViewControllerDataSource>

@property (strong, retain) Ad* ad;
@property (strong, nonatomic) UIPageViewController *pageViewController;

@end

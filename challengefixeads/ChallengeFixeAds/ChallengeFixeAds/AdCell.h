//
//  AdCell.h
//  ChallengeFixeAds
//
//  Created by Carlos Manuel Garcia Neto on 25/12/15.
//  Copyright © 2015 Carlos Manuel Garcia Neto. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "Ad.h"

@interface AdCell : UITableViewCell

@property (nonatomic, strong) UIViewController *parent;
@property (strong, nonatomic) Ad* ad;
extern NSMutableArray* favorits;

@property (weak, nonatomic) IBOutlet UIImageView *adImage;
@property (weak, nonatomic) IBOutlet UITextView *titleTextView;
@property (weak, nonatomic) IBOutlet UILabel *cityLabel;
@property (weak, nonatomic) IBOutlet UILabel *createdLabel;
@property (weak, nonatomic) IBOutlet UILabel *priceLabel;
@property (weak, nonatomic) IBOutlet UIButton *markBtn;

- (IBAction)shareButton:(UIButton *)sender;
- (void) updateUI;

@end

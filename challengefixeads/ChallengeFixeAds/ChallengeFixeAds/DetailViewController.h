//
//  DetailViewController.h
//  ChallengeFixeAds
//
//  Created by Carlos Manuel Garcia Neto on 23/12/15.
//  Copyright © 2015 Carlos Manuel Garcia Neto. All rights reserved.
//

#import "UILabel+Utils.h"
#import "Ad.h"
#import <SDWebImage/UIImageView+WebCache.h>
#import "MapVC.h"
#import "GalleryVC.h"

@interface DetailViewController : UIViewController

@property NSUInteger index;
@property (nonatomic, strong) NSArray<Ad*>* ads;
extern NSMutableArray* favorits;

@property (strong, nonatomic) IBOutlet UIView *detailView;
@property (weak, nonatomic) IBOutlet UIImageView *adPhoto;
@property (weak, nonatomic) IBOutlet UILabel *titleLabel;
@property (weak, nonatomic) IBOutlet UILabel *priceLabel;
@property (weak, nonatomic) IBOutlet UILabel *bgLocal;
@property (weak, nonatomic) IBOutlet UILabel *locationLabel;
@property (weak, nonatomic) IBOutlet UILabel *createdLabel;
@property (weak, nonatomic) IBOutlet UILabel *detailDescriptionLabel;

@property (weak, nonatomic) IBOutlet UIView *viewBadge;
@property (weak, nonatomic) IBOutlet UILabel *bgPhoto;
@property (weak, nonatomic) IBOutlet UILabel *contadorImagens;
@property (weak, nonatomic) IBOutlet UILabel *bgMark;
@property (weak, nonatomic) IBOutlet UIButton *markBtn;

@end


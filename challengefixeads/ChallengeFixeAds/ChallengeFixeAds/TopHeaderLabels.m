//
//  TopHeaderLabels.m
//  ChallengeFixeAds
//
//  Created by Carlos Manuel Garcia Neto on 23/12/15.
//  Copyright © 2015 Carlos Manuel Garcia Neto. All rights reserved.
//

#import "TopHeaderLabels.h"
#import "NSDictionary+Parser.h"

@implementation TopHeaderLabels


- (instancetype)init:(NSDictionary*)jsonDict
{
    self = [super init];
    if (self) {
        _category_icon = [jsonDict strFromKey:@"category_icon"];
        _location_label = [jsonDict strFromKey:@"location_label"];
    }
    return self;
}

@end

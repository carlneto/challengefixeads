//
//  MasterViewController.m
//  ChallengeFixeAds
//
//  Created by Carlos Manuel Garcia Neto on 23/12/15.
//  Copyright © 2015 Carlos Manuel Garcia Neto. All rights reserved.
//

#import "MasterViewController.h"
#import "DetailViewController.h"
#import "Categoria.h"
#import "AdCell.h"
#import "Utils.h"

@interface MasterViewController ()

@property (strong, nonatomic) IBOutlet UITableView *listTable;
@property NSArray<Ad*> *dataSource;

@end

@implementation MasterViewController
{
    NSString* adCellId;
    NSString* adCellDetailSegue;
}

static NSString* const API = @"https://olx.pt/i2/ads/?json=1&search[category_id]=25";

- (void)viewDidLoad {
    [super viewDidLoad];
    adCellId = @"adCellId";
    adCellDetailSegue = @"adCellDetailSegue";
    self.detailViewController = (DetailViewController *)[[self.splitViewController.viewControllers firstObject] topViewController];
    [self loadData];
}

- (void) loadData {
    [Utils getJsonResponse:API success:^(NSDictionary *responseDict) {
        Categoria* cat = [[Categoria alloc] init:responseDict];
        _dataSource = cat.ads;
        dispatch_async(dispatch_get_main_queue(), ^{//a atualizaçao do UI tem de ser feito na sua thread (main)
            [_listTable reloadData];
            if (IS_IPAD) {
                NSIndexPath *indexPath = [NSIndexPath indexPathForRow:0 inSection:0];
                [_listTable selectRowAtIndexPath:indexPath animated:NO scrollPosition:UITableViewScrollPositionNone];
                [self performSegueWithIdentifier:adCellDetailSegue sender:self];
            }
        });
    } failure:^(NSError *error) {
        NSLog(@"%@",error);
    }];
}

- (void)viewWillAppear:(BOOL)animated {
    self.clearsSelectionOnViewWillAppear = self.splitViewController.isCollapsed;
    [super viewWillAppear:animated];
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}


#pragma mark - Segues

- (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender {
    if ([[segue identifier] isEqualToString:adCellDetailSegue]) {
        NSIndexPath *indexPath = [_listTable indexPathForSelectedRow];
        DetailViewController *controller = (DetailViewController *)[[segue destinationViewController] topViewController];
        controller.index = indexPath.row;
        controller.ads = _dataSource;
        controller.navigationItem.leftBarButtonItem = self.splitViewController.displayModeButtonItem;
        controller.navigationItem.leftItemsSupplementBackButton = YES;
    }
}


#pragma mark - Table View

- (NSInteger)numberOfSectionsInTableView:(UITableView *)tableView {
    return 1;
}

- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section {
    return self.dataSource.count;
}

- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath {
    AdCell *adCell = [tableView dequeueReusableCellWithIdentifier:adCellId forIndexPath:indexPath];
    if (!adCell) {
        adCell = [AdCell new];
    }
    adCell.ad = self.dataSource[indexPath.row];
    adCell.parent = self;
    [adCell updateUI];
    return adCell;
}

- (BOOL)tableView:(UITableView *)tableView canEditRowAtIndexPath:(NSIndexPath *)indexPath {
    // Return NO if you do not want the specified item to be editable.
    return YES;
}

@end

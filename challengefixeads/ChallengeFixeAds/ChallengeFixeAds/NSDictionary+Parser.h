//
//  NSDictionary+Parser.h
//  ChallengeFixeAds
//
//  Created by Carlos Manuel Garcia Neto on 23/12/15.
//  Copyright © 2015 Carlos Manuel Garcia Neto. All rights reserved.
//
#import <Foundation/Foundation.h>

@interface NSDictionary (Parser)

#define NSNullToNil(arg) ([arg isKindOfClass:[NSNull class]] ? nil : arg)

- (id) objectFromKey:(NSString*)key;
- (BOOL) boolFromKey:(NSString*)key ;
- (NSNumber*) intFromKey:(NSString*)key;
- (NSNumber*) doubleFromKey:(NSString*)key;
- (NSString*) strFromKey:(NSString*)key;
- (NSDictionary*) dictFromKey:(NSString*)key;
- (NSArray*) arrayFromKey:(NSString*)key;
- (BOOL) isNotEmptyDict;

@end

//
//  TopHeaderLabels.h
//  ChallengeFixeAds
//
//  Created by Carlos Manuel Garcia Neto on 23/12/15.
//  Copyright © 2015 Carlos Manuel Garcia Neto. All rights reserved.
//

#import <Foundation/Foundation.h>

@interface TopHeaderLabels : NSObject

@property (nonatomic, strong) NSString* category_icon;
@property (nonatomic, strong) NSString* location_label;

- (instancetype)init:(NSDictionary*)jsonDict;

@end

//
//  DetailViewController.m
//  ChallengeFixeAds
//
//  Created by Carlos Manuel Garcia Neto on 23/12/15.
//  Copyright © 2015 Carlos Manuel Garcia Neto. All rights reserved.
//

#import "DetailViewController.h"
#import "Utils.h"

@interface DetailViewController ()

@end

static NSString* segueToMap = @"segueToMap";
static NSString* segueToGallery = @"segueToGallery";

@implementation DetailViewController
{
    Ad* ad;
    NSUInteger adsCount;
}

@synthesize index, ads, adPhoto, titleLabel, priceLabel, bgLocal, locationLabel, createdLabel,detailDescriptionLabel, viewBadge, bgPhoto, contadorImagens, bgMark, markBtn;

#pragma mark - Managing the detail item

- (void)setDetailItem:(id)newDetailItem
{
    if (ad != newDetailItem) {
        ad = newDetailItem;
        [self configureView];
    }
}

- (void)viewDidLoad
{
    [super viewDidLoad];
    adsCount = ads.count;
    ad = ads[[self normIndex:index]];
    UISwipeGestureRecognizer *gestureRecognizerRight = [[UISwipeGestureRecognizer alloc] initWithTarget:self action:@selector(swipeHandlerRight:)];
    [gestureRecognizerRight setDirection:(UISwipeGestureRecognizerDirectionRight)];
    [self.view addGestureRecognizer:gestureRecognizerRight];
    UISwipeGestureRecognizer *gestureRecognizerLeft = [[UISwipeGestureRecognizer alloc] initWithTarget:self action:@selector(swipeHandlerLeft:)];
    [gestureRecognizerLeft setDirection:(UISwipeGestureRecognizerDirectionLeft)];
    [self.view addGestureRecognizer:gestureRecognizerLeft];
    [self configureView];
}

- (NSUInteger)normIndex:(NSUInteger)idx
{
    return MIN(MAX(0, idx), adsCount-1);
}

-(void)swipeHandlerRight:(UISwipeGestureRecognizer *)recognizer {
    index--;
    [self swipePageRight:YES];
}

-(void)swipeHandlerLeft:(UISwipeGestureRecognizer *)recognizer {
    index++;
    [self swipePageRight:NO];
}

- (void)swipePageRight:(BOOL)io
{
    UIViewAnimationOptions dir = io ? UIViewAnimationOptionTransitionFlipFromRight : UIViewAnimationOptionTransitionFlipFromLeft;
    UIViewAnimationOptions undir = io ? UIViewAnimationOptionTransitionFlipFromLeft : UIViewAnimationOptionTransitionFlipFromRight;
    index = [self normIndex:index];
    [self viewTransitionDirection:dir isFirstPart:YES];
    Ad* newDetailItem = ads[index];
    [self setDetailItem:newDetailItem];
    [self viewTransitionDirection:undir isFirstPart:NO];
}

- (void) viewTransitionDirection:(UIViewAnimationOptions)dir isFirstPart:(BOOL)io
{
    _detailView.alpha=io ? 1.0 : 0.0;
    _detailView.hidden=!io;
    [UIView transitionWithView:_detailView duration:1.5 options:dir animations:
     ^{
         _detailView.hidden=io;
         _detailView.alpha=io ? 0.0 : 1.0;
     } completion:nil];
}

- (void)configureView {
    // Update the user interface for the detail item.
    if (ad) {
        [bgLocal roundCornersofWithValue:2];
        [bgMark roundCornersofWithValue:2];
        [bgPhoto roundCornersofWithValue:2];
        NSURL* photoUrl = [ad.photos urlForPhotoWithWidth:SCREEN_WIDTH andHeight:SCREEN_HEIGHT];
        if (photoUrl) {
            [adPhoto sd_setImageWithURL:photoUrl];
        }
        [titleLabel setText:ad.title];
        [priceLabel setText:ad.list_label];
        [locationLabel setText:ad.city_label];
        [createdLabel setText:ad.created];
        [detailDescriptionLabel setText:ad.descricao];
        NSString* contador = [[NSString alloc]initWithFormat:@"%lu",(unsigned long)ad.photos.data.count];
        [contadorImagens setText:contador];
        [detailDescriptionLabel performFade:1.2];
        [self setImage];
    }
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

#pragma mark - Navigation
-(void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender
{
    if ([[segue identifier] isEqualToString:segueToMap]) {
        MapVC *controller = (MapVC *)[segue destinationViewController];
        [controller setAd:ad];
    } else if ([[segue identifier] isEqualToString:segueToGallery]) {
        GalleryVC *controller = (GalleryVC *)[segue destinationViewController];
        controller.ad = ad;
    }
}

- (IBAction)markBtn:(UIButton *)sender {
    ad.isSelected = !ad.isSelected;
    [self setImage];
}

- (void)setImage{
    UIImage *marked = [UIImage imageNamed:@"marked"];
    [markBtn setImage:marked forState:UIControlStateSelected];
    UIImage *mark = [UIImage imageNamed:@"mark"];
    [markBtn setImage:mark forState:UIControlStateNormal];
    [markBtn setSelected:ad.isSelected];
    [favorits removeObject:ad.ad_id];
    if (ad.isSelected) {
        [favorits addObject:ad.ad_id];
    }
    
}

@end

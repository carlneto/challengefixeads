//
//  PageContentVC.m
//  ChallengeFixeAds
//
//  Created by Carlos Manuel Garcia Neto on 26/12/15.
//  Copyright © 2015 Carlos Manuel Garcia Neto. All rights reserved.
//

#import "PageContentVC.h"
#import <SDWebImage/UIImageView+WebCache.h>

@implementation PageContentVC

- (id)initWithNibName:(NSString *)nibNameOrNil bundle:(NSBundle *)nibBundleOrNil
{
    self = [super initWithNibName:nibNameOrNil bundle:nibBundleOrNil];
    if (self) {
        // Custom initialization
    }
    return self;
}

- (void)viewDidLoad
{
    [super viewDidLoad];
    if (self.imageUrl) {
        [self.imageView sd_setImageWithURL:self.imageUrl];
    }
}

- (void)didReceiveMemoryWarning
{
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

@end

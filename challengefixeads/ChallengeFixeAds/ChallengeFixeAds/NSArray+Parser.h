//
//  NSArray+Parser.h
//  ChallengeFixeAds
//
//  Created by Carlos Manuel Garcia Neto on 24/12/15.
//  Copyright © 2015 Carlos Manuel Garcia Neto. All rights reserved.
//

#import <Foundation/Foundation.h>

@interface NSArray (Parser)

- (BOOL)isEmpty;

@end

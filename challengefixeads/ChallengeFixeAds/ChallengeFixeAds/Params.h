//
//  Params.h
//  ChallengeFixeAds
//
//  Created by Carlos Manuel Garcia Neto on 23/12/15.
//  Copyright © 2015 Carlos Manuel Garcia Neto. All rights reserved.
//

#import <Foundation/Foundation.h>

@interface Params : NSObject

@property (nonatomic, strong) NSString* region;
@property (nonatomic, strong) NSString* subregion;
@property (nonatomic, strong) NSString* category;
@property (nonatomic, strong) NSString* shopId;
@property (nonatomic, strong) NSString* city;
@property (nonatomic, strong) NSDictionary* params;

- (instancetype)init:(NSDictionary*)jsonDict;

@end

//
//  Adverts.h
//  ChallengeFixeAds
//
//  Created by Carlos Manuel Garcia Neto on 23/12/15.
//  Copyright © 2015 Carlos Manuel Garcia Neto. All rights reserved.
//

#import <Foundation/Foundation.h>

@interface Adverts : NSObject

@property (nonatomic) BOOL facebook;
@property (nonatomic) BOOL adMob;
@property (nonatomic, strong) NSString* adMobAdCampaign;
@property (nonatomic, strong) NSString* adMobListingCampaign;
@property (nonatomic, strong) NSString* adMobId;

- (instancetype)init:(NSDictionary*)jsonDict;

@end

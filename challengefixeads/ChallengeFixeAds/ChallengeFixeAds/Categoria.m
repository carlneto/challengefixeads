//
//  Categoria.m
//  ChallengeFixeAds
//
//  Created by Carlos Manuel Garcia Neto on 23/12/15.
//  Copyright © 2015 Carlos Manuel Garcia Neto. All rights reserved.
//

#import "Categoria.h"
#import "NSDictionary+Parser.h"
#import "NSArray+Parser.h"

@implementation Categoria

- (instancetype)init:(NSDictionary*)jsonDict
{
    self = [super init];
    if (self) {
        _category_id = [jsonDict strFromKey:@"category_id"];
        _params = [[Params alloc] init:[jsonDict dictFromKey:@"params"]];
        _total_ads = [jsonDict intFromKey:@"total_ads"];
        _top_header_labels = [[TopHeaderLabels alloc] init:[jsonDict dictFromKey:@"top_header_labels"]];
        _page = [jsonDict intFromKey:@"page"];
        _total_pages = [jsonDict intFromKey:@"total_pages"];
        _ads_on_page = [jsonDict intFromKey:@"ads_on_page"];
        _view = [jsonDict strFromKey:@"view"];
        _next_page_url = [jsonDict strFromKey:@"next_page_url"];
        _ads = [self objsFromArray:[jsonDict arrayFromKey:@"ads"]];
        _adverts = [[Adverts alloc] init:[jsonDict dictFromKey:@"adverts"]];
    }
    return self;
}

- (NSArray<Ad*>*)objsFromArray:(NSArray*)jsonArray {
    if ([jsonArray isEmpty]) {
        return nil;
    }
    NSMutableArray *arr = [NSMutableArray new];
    for (NSDictionary* jsonDict in jsonArray) {
        Ad* obj = [[Ad alloc] init:jsonDict];
        if (obj) {
            [arr addObject:obj];
        }
    }
    return (arr.count ? arr : nil);
}

@end

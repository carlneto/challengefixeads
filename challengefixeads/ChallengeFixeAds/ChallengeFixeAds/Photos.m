//
//  Photos.m
//  ChallengeFixeAds
//
//  Created by Carlos Manuel Garcia Neto on 23/12/15.
//  Copyright © 2015 Carlos Manuel Garcia Neto. All rights reserved.
//

#import "Photos.h"
#import "NSDictionary+Parser.h"
#import "NSArray+Parser.h"

@implementation Photos

static NSString* const PhotoUrl = @"http://img.olx.pt/images_olxpt/";

- (instancetype)init:(NSDictionary*)jsonDict
{
    self = [super init];
    if (self) {
        _riak_ring = [jsonDict intFromKey:@"riak_ring"];
        _riak_key = [jsonDict intFromKey:@"riak_key"];
        _riak_rev = [jsonDict intFromKey:@"riak_rev"];
        _data = [self objsFromArray:[jsonDict arrayFromKey:@"data"]];
    }
    return self;
}

- (NSArray<PhotoData*>*)objsFromArray:(NSArray*)jsonArray {
    if ([jsonArray isEmpty]) {
        return nil;
    }
    NSMutableArray *arr = [NSMutableArray new];
    for (NSDictionary* jsonDict in jsonArray) {
        PhotoData* obj = [[PhotoData alloc] init:jsonDict];
        if (obj) {
            [arr addObject:obj];
        }
    }
    return (arr.count ? arr : nil);
}

#pragma mark - Build photo uri

- (NSURL*) urlForPhotoWithWidth:(NSInteger) w andHeight:(NSInteger) h
{
    return [self urlForPhotoWithWidth:w andHeight:h atIndex:0];
}

- (NSURL*)urlForPhotoWithWidth:(NSInteger)w andHeight:(NSInteger)h atIndex:(NSUInteger)index
{
    PhotoData* actual = _data[index];
    if (!_riak_key || !actual.slot_id) {
        return nil;
    }
    NSString* urlStr = [NSString stringWithFormat:@"%@%@_%@_%ldx%ld.jpg", PhotoUrl, _riak_key, actual.slot_id, (long)w, (long)h];// first.slot_w, first.slot_h]
                                                                                                                                //    NSLog(@"%@",urlStr);
    return [[NSURL alloc] initWithString:urlStr];
}

@end

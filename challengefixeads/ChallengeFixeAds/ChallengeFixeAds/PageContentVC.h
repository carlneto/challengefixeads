//
//  PageContentVC.h
//  ChallengeFixeAds
//
//  Created by Carlos Manuel Garcia Neto on 26/12/15.
//  Copyright © 2015 Carlos Manuel Garcia Neto. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface PageContentVC : UIViewController

@property NSUInteger pageIndex;
@property NSURL *imageUrl;
@property (weak, nonatomic) IBOutlet UIImageView *imageView;

@end

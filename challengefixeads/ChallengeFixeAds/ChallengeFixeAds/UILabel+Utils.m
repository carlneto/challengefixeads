//
//  UILabel+Utils.m
//  ChallengeFixeAds
//
//  Created by Carlos Manuel Garcia Neto on 26/12/15.
//  Copyright © 2015 Carlos Manuel Garcia Neto. All rights reserved.
//

#import "UILabel+Utils.h"

@implementation UILabel (Utils)

- (void)roundCornersofWithValue:(NSInteger)value
{
    CALayer * lp = [self layer];
    [lp setMasksToBounds:YES];
    [lp setCornerRadius:self.frame.size.width/value];
}

- (void)performFade:(NSTimeInterval)delay
{
    [self performSelector:@selector(fadeText) withObject:nil afterDelay:delay];
}

- (void)fadeText
{
    [UIView animateWithDuration:1.2 animations:^(void) {
        UIColor* color = [UIColor colorWithWhite:1.0 alpha:0.6];
        self.backgroundColor = color;
    }];
}

@end

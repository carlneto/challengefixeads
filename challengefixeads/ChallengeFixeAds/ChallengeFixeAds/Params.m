//
//  Params.m
//  ChallengeFixeAds
//
//  Created by Carlos Manuel Garcia Neto on 23/12/15.
//  Copyright © 2015 Carlos Manuel Garcia Neto. All rights reserved.
//

#import "Params.h"
#import "NSDictionary+Parser.h"

@implementation Params

- (instancetype)init:(NSDictionary*)jsonDict
{
    self = [super init];
    if (self) {
        _region = [jsonDict strFromKey:@"region"];
        _subregion = [jsonDict strFromKey:@"subregion"];
        _category = [jsonDict strFromKey:@"category"];
        _shopId = [jsonDict strFromKey:@"shopId"];
        _city = [jsonDict strFromKey:@"city"];
        _params = [jsonDict dictFromKey:@"params"];
    }
    return self;
}

@end

//
//  NSArray+Parser.m
//  ChallengeFixeAds
//
//  Created by Carlos Manuel Garcia Neto on 24/12/15.
//  Copyright © 2015 Carlos Manuel Garcia Neto. All rights reserved.
//

#import "NSArray+Parser.h"

@implementation NSArray (Parser)

- (BOOL)isEmpty {
    return self == (id)[NSNull null] || [self count] == 0;
}

@end

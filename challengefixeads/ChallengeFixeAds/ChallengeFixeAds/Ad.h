//
//  Ad.h
//  ChallengeFixeAds
//
//  Created by Carlos Manuel Garcia Neto on 23/12/15.
//  Copyright © 2015 Carlos Manuel Garcia Neto. All rights reserved.
//

#import <Foundation/Foundation.h>
#import "Photos.h"

@interface Ad : NSObject

@property (nonatomic, strong) NSString* ad_id;
@property (nonatomic, strong) NSString* url;
@property (nonatomic, strong) NSString* preview_url;
@property (nonatomic, strong) NSString* title;
@property (nonatomic, strong) NSString* created;
@property (nonatomic, strong) NSNumber* age;
@property (nonatomic, strong) NSString* descricao;
@property (nonatomic) BOOL highlighted;
@property (nonatomic) BOOL urgent;
@property (nonatomic) BOOL topAd;
@property (nonatomic, strong) NSString* category_id;
@property (nonatomic, strong) NSDictionary* params;
@property (nonatomic, strong) NSArray* subtitle;
@property (nonatomic, strong) NSString* business;
@property (nonatomic) BOOL hide_user_ads_button;
@property (nonatomic, strong) NSString* status;
@property (nonatomic, strong) NSString* header;
@property (nonatomic, strong) NSString* header_type;
@property (nonatomic) BOOL has_phone;
@property (nonatomic) BOOL has_email;
@property (nonatomic) BOOL is_price_negotiable;
@property (nonatomic, strong) NSNumber* map_zoom;
@property (nonatomic, strong) NSNumber* map_lat;
@property (nonatomic, strong) NSNumber* map_lon;
@property (nonatomic, strong) NSNumber* map_radius;
@property (nonatomic) BOOL map_show_detailed;
@property (nonatomic, strong) NSString* map_location;
@property (nonatomic, strong) NSString* city_label;
@property (nonatomic, strong) NSString* person;
@property (nonatomic, strong) NSString* user_label;
@property (nonatomic, strong) NSString* user_ads_id;
@property (nonatomic, strong) NSString* user_id;
@property (nonatomic, strong) NSString* numeric_user_id;
@property (nonatomic, strong) NSString* user_ads_url;
@property (nonatomic, strong) NSString* list_label;
@property (nonatomic, strong) NSString* list_label_ad;
@property (nonatomic, strong) NSString* list_label_small;
@property (nonatomic, strong) Photos* photos;
@property (nonatomic, strong) NSString* chat_options;

@property (nonatomic) BOOL isSelected;
extern NSMutableArray* favorits;

- (instancetype)init:(NSDictionary*)jsonDict;

@end

//
//  NSDictionary+Parser.m
//  ChallengeFixeAds
//
//  Created by Carlos Manuel Garcia Neto on 23/12/15.
//  Copyright © 2015 Carlos Manuel Garcia Neto. All rights reserved.
//

#import "NSDictionary+Parser.h"
#import "Utils.h"

@implementation NSDictionary (Parser)

- (id) objectFromKey:(NSString*)key{
    if ([Utils isNotEmptyString:key] && [self isNotEmptyDict]) {
        return NSNullToNil([self objectForKey:key]);
    }
    return nil;
}

- (BOOL) boolFromKey:(NSString*)key {
    return [[self objectFromKey:key] boolValue];
}

- (NSNumber*) intFromKey:(NSString*)key {
    NSInteger integer = [[self objectFromKey:key] integerValue];
    return [[NSNumber alloc]initWithInteger:integer];
}

- (NSNumber*) doubleFromKey:(NSString*)key {
    double dbl = [[self objectFromKey:key] doubleValue];
    return [[NSNumber alloc]initWithDouble:dbl];
}

- (NSString*) strFromKey:(NSString*)key {
    return [self objectFromKey:key];
}

- (NSDictionary*) dictFromKey:(NSString*)key {
    return [self objectFromKey:key];
}

- (NSArray*) arrayFromKey:(NSString*)key {
    return [self objectFromKey:key];
}

- (BOOL) isNotEmptyDict {
    return self && self != (id)[NSNull null];
}


@end

//
//  Categoria.h
//  ChallengeFixeAds
//
//  Created by Carlos Manuel Garcia Neto on 23/12/15.
//  Copyright © 2015 Carlos Manuel Garcia Neto. All rights reserved.
//

#import <Foundation/Foundation.h>
#import "Params.h"
#import "Ad.h"
#import "Adverts.h"
#import "TopHeaderLabels.h"

@interface Categoria : NSObject

@property (nonatomic, strong) NSString* category_id;
@property (nonatomic, strong) Params* params;
@property (nonatomic, strong) NSNumber* total_ads;
@property (nonatomic, strong) TopHeaderLabels* top_header_labels;
@property (nonatomic, strong) NSNumber* page;
@property (nonatomic, strong) NSNumber* total_pages;
@property (nonatomic, strong) NSNumber* ads_on_page;
@property (nonatomic, strong) NSString* view;
@property (nonatomic, strong) NSString* next_page_url;
@property (nonatomic, strong) NSArray<Ad*>* ads;
@property (nonatomic, strong) Adverts* adverts;

- (instancetype)init:(NSDictionary*)jsonDict;

@end

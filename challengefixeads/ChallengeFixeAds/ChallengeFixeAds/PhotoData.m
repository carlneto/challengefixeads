//
//  PhotoData.m
//  ChallengeFixeAds
//
//  Created by Carlos Manuel Garcia Neto on 23/12/15.
//  Copyright © 2015 Carlos Manuel Garcia Neto. All rights reserved.
//

#import "PhotoData.h"
#import "NSDictionary+Parser.h"

@implementation PhotoData

- (instancetype)init:(NSDictionary*)jsonDict
{
    self = [super init];
    if (self) {
        _slot_id = [jsonDict intFromKey:@"slot_id"];
        _slot_w = [jsonDict intFromKey:@"slot_w"];
        _slot_h = [jsonDict intFromKey:@"slot_h"];
    }
    return self;
}

@end

//
//  Photos.h
//  ChallengeFixeAds
//
//  Created by Carlos Manuel Garcia Neto on 23/12/15.
//  Copyright © 2015 Carlos Manuel Garcia Neto. All rights reserved.
//

#import <Foundation/Foundation.h>
#include "PhotoData.h"

@interface Photos : NSObject

@property (nonatomic, strong) NSNumber* riak_ring;
@property (nonatomic, strong) NSNumber* riak_key;
@property (nonatomic, strong) NSNumber* riak_rev;
@property (nonatomic, strong) NSArray<PhotoData*>* data;

- (instancetype)init:(NSDictionary*)jsonDict;
- (NSURL*)urlForPhotoWithWidth:(NSInteger)w andHeight:(NSInteger)h;
- (NSURL*)urlForPhotoWithWidth:(NSInteger)w andHeight:(NSInteger)h atIndex:(NSUInteger)index;

@end

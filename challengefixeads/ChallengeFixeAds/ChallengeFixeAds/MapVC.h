//
//  MapVC.h
//  ChallengeFixeAds
//
//  Created by Carlos Manuel Garcia Neto on 26/12/15.
//  Copyright © 2015 Carlos Manuel Garcia Neto. All rights reserved.
//

#import <MapKit/MapKit.h>
#import "Ad.h"

@interface MapVC : UIViewController<MKMapViewDelegate>

@property (strong, retain) Ad* ad;
@property (strong, nonatomic) IBOutlet MKMapView *mapView;

- (void)initializeWithOLXAd:(Ad*)obj;

@end

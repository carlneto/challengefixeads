//
//  Utils.m
//  ChallengeFixeAds
//
//  Created by Carlos Manuel Garcia Neto on 23/12/15.
//  Copyright © 2015 Carlos Manuel Garcia Neto. All rights reserved.
//

#import "Utils.h"

@implementation Utils


+ (BOOL) isNotEmptyString:(NSString *) txt {
    return [self isNotNullObj:txt] && txt.length > 0;
}

+ (BOOL) isNotNullObj:(NSObject *) obj {
    return obj && obj != (id)[NSNull null];
}

+ (void) getJsonResponse:(NSString*)urlStr success:(void (^)(NSDictionary *responseDict))success failure:(void(^)(NSError*error))failure {
    NSURLSession* session = [NSURLSession sharedSession];
    NSURL* url = [NSURL URLWithString: urlStr];
    NSURLSessionDataTask* dataTask = [session dataTaskWithURL:url completionHandler: ^(NSData *data, NSURLResponse *response, NSError *error) {
        NSDictionary* json = [NSJSONSerialization JSONObjectWithData:data options:0 error:nil];
        success(json);
    }];
    [dataTask resume];
}

#pragma mark - user defaults

+ (void) saveAdFavorit:(NSString *)ad_id
{
    NSUserDefaults *defaults = [NSUserDefaults standardUserDefaults];
    [defaults setValue:ad_id forKey:@"ad_id"];
    [defaults synchronize];
}

+ (BOOL)hasAdFavorit:(NSString *)ad_id
{
    NSUserDefaults *defaults = [NSUserDefaults standardUserDefaults];
    NSString *votedOption = [defaults valueForKey:@"ad_id"];
    return votedOption;
}

+ (void)resetDefaults {
    NSUserDefaults * defs = [NSUserDefaults standardUserDefaults];
    NSDictionary * dict = [defs dictionaryRepresentation];
    for (id key in dict) {
        [defs removeObjectForKey:key];
    }
    [defs synchronize];
}
//
//
//+ (void) setCategoryColor:(NSDictionary *) dic {
//    //    [Utils resetDefaults];
//    NSUserDefaults *defaults = [NSUserDefaults standardUserDefaults];
//    [defaults setObject:dic forKey:TAG_CATS];
//    [defaults synchronize];
//}
//
//
//
//+ (UIColor *) getColorOfCategory:(NSString *) category {
//    NSUserDefaults *defaults = [NSUserDefaults standardUserDefaults];
//    NSDictionary *catsDic = [defaults objectForKey:TAG_CATS];
//    
//    category = [Utils normalizeString:category];
//    
//    NSString *catHexa = [catsDic valueForKey:category];
//    
//    if (catHexa) {
//        return [self colorWithHexString:catHexa];
//    } else {
//        return [UIColor redColor];
//    }
//}
//
//+ (UIColor *) getColorFrom:(NSArray*)titulos {
//    NSUserDefaults *defaults = [NSUserDefaults standardUserDefaults];
//    NSDictionary *catsDic = [defaults objectForKey:TAG_CATS];
//    for (NSString* titulo in titulos) {
//        NSString* title = [Utils normalizeString:titulo];
//        NSString *color = [catsDic valueForKey:title];
//        if (color) {
//            return [self colorWithHexString:color];
//        }
//    }
//    return [Utils colorWithHexString:COLOR_RED_TVI_HEX];
//}
//
//+ (UIImage *)imageFromColor:(UIColor *)color
//{
//    CGRect rect = CGRectMake(0.0f, 0.0f, 1.0f, 1.0f);
//    UIGraphicsBeginImageContext(rect.size);
//    CGContextRef context = UIGraphicsGetCurrentContext();
//    
//    CGContextSetFillColorWithColor(context, [color CGColor]);
//    CGContextFillRect(context, rect);
//    
//    UIImage *image = UIGraphicsGetImageFromCurrentImageContext();
//    UIGraphicsEndImageContext();
//    
//    return image;
//}



@end

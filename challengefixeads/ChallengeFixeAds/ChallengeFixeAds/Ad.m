//
//  Ad.m
//  ChallengeFixeAds
//
//  Created by Carlos Manuel Garcia Neto on 23/12/15.
//  Copyright © 2015 Carlos Manuel Garcia Neto. All rights reserved.
//

#import "Ad.h"
#import "NSDictionary+Parser.h"
#import "NSArray+Parser.h"
#import "Params.h"

@implementation Ad

- (instancetype)init:(NSDictionary*)jsonDict
{
    self = [super init];
    if (self) {
        _ad_id = [jsonDict strFromKey:@"id"];
        _isSelected = NO;
        for (NSString* fav_id in favorits) {
            if ([fav_id isEqualToString:_ad_id]) {
                _isSelected = YES;
                break;
            }
        }
        _url = [jsonDict strFromKey:@"url"];
        _preview_url = [jsonDict strFromKey:@"preview_url"];
        _title = [jsonDict strFromKey:@"title"];
        _created = [jsonDict strFromKey:@"created"];
        _age = [jsonDict intFromKey:@"age"];
        _descricao = [jsonDict strFromKey:@"description"];
        _highlighted = [jsonDict boolFromKey:@"highlighted"];
        _urgent = [jsonDict boolFromKey:@"urgent"];
        _topAd = [jsonDict boolFromKey:@"topAd"];
        _category_id = [jsonDict strFromKey:@"category_id"];
        _params = [jsonDict dictFromKey:@"params"];
        _subtitle = [jsonDict arrayFromKey:@"subtitle"];
        _business = [jsonDict strFromKey:@"business"];
        _hide_user_ads_button = [jsonDict boolFromKey:@"hide_user_ads_button"];
        _status = [jsonDict strFromKey:@"status"];
        _header = [jsonDict strFromKey:@"header"];
        _header_type = [jsonDict strFromKey:@"header_type"];
        _has_phone = [jsonDict boolFromKey:@"has_phone"];
        _has_email = [jsonDict boolFromKey:@"has_email"];
        _is_price_negotiable = [jsonDict boolFromKey:@"is_price_negotiable"];
        _map_zoom = [jsonDict intFromKey:@"map_zoom"];
        _map_lat = [jsonDict doubleFromKey:@"map_lat"];
        _map_lon = [jsonDict doubleFromKey:@"map_lon"];
        _map_radius = [jsonDict intFromKey:@"map_radius"];
        _map_show_detailed = [jsonDict boolFromKey:@"map_show_detailed"];
        _map_location = [jsonDict strFromKey:@"map_location"];
        _city_label = [jsonDict strFromKey:@"city_label"];
        _person = [jsonDict strFromKey:@"person"];
        _user_label = [jsonDict strFromKey:@"user_label"];
        _user_ads_id = [jsonDict strFromKey:@"user_ads_id"];
        _user_id = [jsonDict strFromKey:@"user_id"];
        _numeric_user_id = [jsonDict strFromKey:@"numeric_user_id"];
        _user_ads_url = [jsonDict strFromKey:@"user_ads_url"];
        _list_label = [jsonDict strFromKey:@"list_label"];
        _list_label_ad = [jsonDict strFromKey:@"list_label_ad"];
        _list_label_small = [jsonDict strFromKey:@"list_label_small"];
        _photos = [[Photos alloc] init:[jsonDict dictFromKey:@"photos"]];
        _chat_options = [jsonDict strFromKey:@"chat_options"];
    }
    return self;
}

@end

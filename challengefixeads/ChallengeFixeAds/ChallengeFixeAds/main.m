//
//  main.m
//  ChallengeFixeAds
//
//  Created by Carlos Manuel Garcia Neto on 23/12/15.
//  Copyright © 2015 Carlos Manuel Garcia Neto. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "AppDelegate.h"

NSMutableArray* favorits;

int main(int argc, char * argv[]) {
    @autoreleasepool {
        return UIApplicationMain(argc, argv, nil, NSStringFromClass([AppDelegate class]));
    }
}

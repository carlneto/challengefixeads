//
//  Adverts.m
//  ChallengeFixeAds
//
//  Created by Carlos Manuel Garcia Neto on 23/12/15.
//  Copyright © 2015 Carlos Manuel Garcia Neto. All rights reserved.
//

#import "Adverts.h"
#import "NSDictionary+Parser.h"

@implementation Adverts

- (instancetype)init:(NSDictionary*)jsonDict
{
    self = [super init];
    if (self && jsonDict.count) {
        _facebook = [jsonDict boolFromKey:@"facebook"];
        _adMob = [jsonDict boolFromKey:@"adMob"];
        _adMobAdCampaign = [jsonDict strFromKey:@"adMobAdCampaign"];
        _adMobListingCampaign = [jsonDict strFromKey:@"adMobListingCampaign"];
        _adMobId = [jsonDict strFromKey:@"adMobId"];
    }
    return self;
}
@end

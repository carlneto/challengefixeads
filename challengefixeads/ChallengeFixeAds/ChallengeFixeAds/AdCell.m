//
//  AdCell.m
//  ChallengeFixeAds
//
//  Created by Carlos Manuel Garcia Neto on 25/12/15.
//  Copyright © 2015 Carlos Manuel Garcia Neto. All rights reserved.
//

#import "AdCell.h"
#import <SDWebImage/UIImageView+WebCache.h>
#import "Utils.h"

@implementation AdCell

@synthesize ad, adImage, titleTextView, cityLabel, createdLabel, priceLabel;

- (void) updateUI
{
    NSURL* photoUrl = [ad.photos urlForPhotoWithWidth:100 andHeight:100];
    if (photoUrl) {
        [adImage sd_setImageWithURL:photoUrl];
    }
    titleTextView.text = ad.title;
    cityLabel.text = ad.city_label;
    createdLabel.text = ad.created;
    priceLabel.text = ad.list_label;
    [self setImage];
}

- (IBAction)shareButton:(UIButton *)sender
{
    NSString *textToShare = ad.title;
    NSURL *myWebsite = [NSURL URLWithString:ad.url];
    
    NSArray *objectsToShare = @[textToShare, myWebsite];
    
    UIActivityViewController *activityVC = [[UIActivityViewController alloc] initWithActivityItems:objectsToShare applicationActivities:nil];
    
    NSArray *excludeActivities = @[UIActivityTypeAirDrop,
                                   UIActivityTypePrint,
                                   UIActivityTypeAssignToContact,
                                   UIActivityTypeSaveToCameraRoll,
                                   UIActivityTypeAddToReadingList,
                                   UIActivityTypePostToFlickr,
                                   UIActivityTypePostToVimeo];
    
    activityVC.excludedActivityTypes = excludeActivities;
    
    if (IS_IPAD) {
        activityVC.modalPresentationStyle = UIModalPresentationPopover;
        activityVC.popoverPresentationController.sourceView = sender;
    }
    [self.parent presentViewController:activityVC animated:YES completion:nil];
}

- (IBAction)markBtn:(UIButton *)sender {
    ad.isSelected = !ad.isSelected;
    [self setImage];
}

- (void)setImage{
    UIImage *marked = [UIImage imageNamed:@"marked"];
    [_markBtn setImage:marked forState:UIControlStateSelected];
    UIImage *mark = [UIImage imageNamed:@"mark"];
    [_markBtn setImage:mark forState:UIControlStateNormal];
    [_markBtn setSelected:ad.isSelected];
    [favorits removeObject:ad.ad_id];
    if (ad.isSelected) {
        [favorits addObject:ad.ad_id];
    }
}

@end

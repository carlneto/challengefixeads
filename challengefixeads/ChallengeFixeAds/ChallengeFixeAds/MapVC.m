//
//  MapVC.m
//  ChallengeFixeAds
//
//  Created by Carlos Manuel Garcia Neto on 26/12/15.
//  Copyright © 2015 Carlos Manuel Garcia Neto. All rights reserved.
//

#import "MapVC.h"

@implementation MapVC

- (void)viewDidLoad {
    [super viewDidLoad];
//    [[NSBundle mainBundle] loadNibNamed:@"MapPortraitView" owner:self options:nil];
    _mapView.delegate = self;
    [self configureMap];
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];

}

- (void)initializeWithOLXAd:(Ad*)obj {
    _ad = obj;
}

#pragma mark - Map Configuration
- (void)configureMap
{
    double area = _ad.map_zoom.doubleValue / 100;
    MKCoordinateSpan span;
    span.latitudeDelta = area;
    span.longitudeDelta = area;
    
    CLLocationCoordinate2D center;
    center.latitude = _ad.map_lat.doubleValue;
    center.longitude= _ad.map_lon.doubleValue;
   
    MKCoordinateRegion region;
    region.span = span;
    region.center = center;
    [_mapView setRegion:region animated:YES];
    
    MKCircle *circle = [MKCircle circleWithCenterCoordinate:center radius:_ad.map_zoom.intValue * 100];
    [_mapView addOverlay:circle];
}

- (MKOverlayRenderer *)mapView:(MKMapView *)mapView rendererForOverlay:(id < MKOverlay >)overlay
{
    MKCircleRenderer *circleR = [[MKCircleRenderer alloc] initWithCircle:(MKCircle *)overlay];
    circleR.strokeColor = [[UIColor greenColor] colorWithAlphaComponent:0.2];
    circleR.fillColor = [[UIColor greenColor] colorWithAlphaComponent:0.1];
    return circleR;
}

@end
